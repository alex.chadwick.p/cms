import React from "react"
import { Link } from "react-router-dom"

// <Link to=""></Link>

class TopBar extends React.Component {
  render() {
    return (
      <nav class="navbar" role="navigation">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item">
            Chadwick CMS
          </Link>
        </div>
        <div className="navbar-end">
          <div className="navbar-item has-dropdown is-hoverable">
            <Link className="navbar-link" to="#">
              ACTIONS
            </Link>
            <div class="navbar-dropdown">
              <Link to="/addClient" className="navbar-item">
                Add Client
              </Link>
              <Link to="/" className="navbar-item">
                Clients
              </Link>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default TopBar
