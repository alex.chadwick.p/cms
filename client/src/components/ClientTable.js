import React from "react"

class ClientTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      apiResponse: [],
    }
  }

  async callAPI() {
    /**
    fetch("http://localhost:8080/client/list?selectQuery=all")
      .then((res) => {
        console.log(res)
        res.json()
      })
      .then((res) => this.setState({ apiResponse: res }))
    **/
    const response = await fetch(
      "http://localhost:8080/client/list?selectQuery=all"
    )
    const clients = await response.json()

    this.setState({ apiResponse: clients })
  }

  componentDidMount() {
    this.callAPI()
  }

  render() {
    const temp = this.state.apiResponse[0]
    console.log(temp)

    return (
      <div>
        <table className="table is-hoverable is-fullwidth">
          <tbody>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Phone Number</th>
              <th>Email Address</th>
              <th>Website</th>
            </tr>

            {this.state.apiResponse.map((v, i) => (
              <tr>
                <td key={i}>{v.id}</td>
                <td key={i}>
                  {v.first_name} {v.last_name}
                </td>
                <td key={i}>{v.phone_number}</td>
                <td key={i}>{v.email_address}</td>
                <td key={i}>{v.website}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default ClientTable
