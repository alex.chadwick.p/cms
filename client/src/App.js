import "./App.css"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Home from "./components/Home"
import AddClient from "./components/AddClient"
import TopBar from "./components/TopBar"

function App() {
  return (
    <Router>
      <TopBar />
      <Switch>
        <Route path="/addClient">
          <AddClient />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
