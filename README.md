# Chadwick CMS
Chadwick CMS is an open-source Client Management System, free forever.

# Installation
To install Chadwick CMS you must clone this repository to your server. After that you must run `npm start` inside the API folder. Then run `npm start` in the client folder to (this will start in development mode at the moment, so it won't be fully optimised). If there are any errors, check the console. Make sure that ports `3000` and `8080` are not being used by another process. You must have a postgresql database installed too. At the moment you will have to go into `/api/src/models/clientModel.js` and replace the parameters on line 5. I'm aiming to give the ability to use a JSON file to configure ports and logins for your application and database.

# Usage
To use this follow the installation above, this will run both the API and the Client. Once the API is complete I will provide a document with all of the routes that are available for use, to allow developers to program their own client. At the moment the client provided is a web interface. If you successfully run the installation you will be able to go to `http://localhost:3000` to start using the client. The home page will present you all of the clients and on the top right you have a dropdown menu with the available options. Clicking on the top left will take you back to the home page.
