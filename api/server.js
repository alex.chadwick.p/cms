const express = require("express")
const app = express()
const cors = require("cors")

// TODO: Update this to pull port from .env file or from a config.json file
const PORT = 8080

// TODO: Update this to pull log boolean from .env file or from a config.json file
const LOG = true

app.use(cors())

app.use("/client", require("./src/routes/clientsRouter"))

app.listen(PORT, () => {
  if (LOG) console.log(`Application running on http://localhost:${PORT}`)
})
