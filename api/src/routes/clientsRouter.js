const express = require("express")
const router = express.Router()

const controller = require("../controllers/clientsController")

router.get("/list", controller.client_list_get)
router.get("/create", controller.client_create_get)
router.get("/delete", controller.client_delete_get)
router.get("/update", controller.client_update_get)

module.exports = router
