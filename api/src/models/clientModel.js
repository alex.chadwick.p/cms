const { Sequelize, DataTypes } = require("sequelize")
/*const sequelize = new Sequelize(
  "postgres://dev:dev123@localhost:5432/clientmanagementsystem"
)*/
const sequelize = new Sequelize("clientmanagementsystem", "dev", "dev123", {
  host: "localhost",
  dialect: "postgres",
  omitNull: true,
})

const Client = sequelize.define(
  "Client",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phone_number: {
      type: DataTypes.STRING,
    },
    email_address: {
      type: DataTypes.STRING,
    },
    website: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: "Clients",
  }
)

/*
const test = async () => {
  await Client.sync({ alter: true })
  const alex = await Client.create({
    first_name: "Alex",
    last_name: "Chadwick",
    phone_number: "test",
    email_address: "test",
    website: "test",
  })
}
*/

// test()

module.exports = Client
